using DG.Tweening;
using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour
{
    [Tooltip("The hoolding object for the screens.")]
    public RectTransform menuStrip;

    [Tooltip("The various screens.")]
    public List<RectTransform> menuItems;

    private List<float> ScreenPositions;

    [Tooltip("How long it takes to change between scenes.")]
    public float ScreenChangeDuration = 0.33f;

    [Tooltip("Volume slider.")]
    public Slider volumeSlider;

    [Tooltip("Main scene of the game.")]
    [Scene]
    public string mainScene;

    private void Awake()
    {
        volumeSlider.SetValueWithoutNotify(AudioListener.volume);
        volumeSlider.onValueChanged.AddListener((value)=>AudioListener.volume = value);
        ScreenPositions = new List<float>();
        for (int i = 0; i < menuItems.Count; i++)
        {
            ScreenPositions.Add(menuItems[i].anchoredPosition.x);
        }
    }

    public void SetScreen(int index)
    {
        menuStrip.DOAnchorPosX(ScreenPositions[index], ScreenChangeDuration);
    }

    public void StartGame()
    {
        SceneManager.LoadScene(mainScene);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
