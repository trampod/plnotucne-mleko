using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteAfter(typeof(MilkManager))]
[ExecuteAfter(typeof(MilkItem))]
public class MilkDisplay : MonoBehaviour
{
    public HealthBar MilkBar;

    private void Start()
    {
        UpdateDisplay();
        MilkManager.Instance.MilkGrabbed.AddListener(UpdateDisplay);
    }

    public void UpdateDisplay()
    {
        MilkBar.SetHealth(MilkManager.Instance.GrabbedMilks,MilkManager.Instance.MilkToGrab);
    }

    private void OnDestroy()
    {
        MilkManager.Instance.MilkGrabbed.RemoveListener(UpdateDisplay);
    }
}
