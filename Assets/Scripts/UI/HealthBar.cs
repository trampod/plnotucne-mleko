using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class HealthBar : MonoBehaviour
{
    public Slider Slider;
    public TMP_Text Text;

    private Tween SliderTween;

    public void SetHealth(float current, float max)
    {
        if (SliderTween != null)
            SliderTween.Kill();

        Text.text = $"{current}/{max}";
        Slider.DOValue(current / max, 0.3f);
    }
}
