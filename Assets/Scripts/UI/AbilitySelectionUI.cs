using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilitySelectionUI : MonoBehaviour
{
    [Tooltip("List of icons for weapons. Needs to be the same length as number of weapons, in the same order as the number of weapons on player.")]
    public List<Image> weaponIcons = new List<Image>();

    [Tooltip("Icon scale for selected weapon.")]
    public Vector3 DeselectedIconScale;
    [Tooltip("Icon scale for deselected weapons.")]
    public Vector3 SelectedIconScale;

    private int _selectedWeapon;
    private Sequence _selectTween;
    private Sequence _deselectTween;

    private void Awake()
    {
        foreach(var icon in weaponIcons)
        {
            icon.transform.localScale = DeselectedIconScale;
            icon.DOFade(0.5f,0f); //I know, this is awful, but it works
        }
    }

    public void RefreshUI(int weaponID)
    {
        if(_selectTween != null && _selectTween.active)
            _selectTween.Kill();
        if (_deselectTween != null && _deselectTween.active)
            _deselectTween.Kill();

        float curentState = Mathf.InverseLerp(DeselectedIconScale.x, SelectedIconScale.x, weaponIcons[_selectedWeapon].transform.localScale.x);
        float targetDuration = Mathf.Lerp(0, 0.3f, curentState);

        _deselectTween = DOTween.Sequence();
        _selectTween = DOTween.Sequence();

        _deselectTween.Join(weaponIcons[_selectedWeapon].transform.DOScale(DeselectedIconScale, targetDuration));
        _deselectTween.Join(weaponIcons[_selectedWeapon].DOFade(0.5f,targetDuration));
        _selectedWeapon = weaponID;
        _selectTween.Join(weaponIcons[_selectedWeapon].transform.DOScale(SelectedIconScale, targetDuration));
        _selectTween.Join(weaponIcons[_selectedWeapon].DOFade(1f, targetDuration));
    }
}
