using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WobbleUI : MonoBehaviour
{
    public float UpDownChange = 0.5f;
    public float UpDownDuration = 4f;
    public float TiltChange = 30f;
    public float TiltDuration = 3f;

    private void Awake()
    {
        RectTransform rectTransform = GetComponent<RectTransform>();

        rectTransform.DOAnchorPosY(rectTransform.anchoredPosition.y + (UpDownChange / 2f), UpDownDuration / 2f)
            .SetEase(Ease.InOutQuad)
            .OnComplete(() =>
            {
                rectTransform.DOAnchorPos3DY(rectTransform.anchoredPosition.y - (UpDownChange / 2f), UpDownDuration / 2f)
                .SetLoops(-1, LoopType.Yoyo)
                .SetEase(Ease.InOutQuad);
            });

        Vector3 firstRotation = rectTransform.eulerAngles;
        firstRotation.z += TiltChange / 2f;
        Vector3 secondRotation = rectTransform.eulerAngles;
        secondRotation.z -= TiltChange / 2f;

        rectTransform.DORotate(firstRotation, TiltDuration / 2f)
            .SetEase(Ease.InOutQuad)
            .OnComplete(() =>
            {
                rectTransform.DORotate(secondRotation, TiltDuration / 2f)
                .SetLoops(-1, LoopType.Yoyo)
                .SetEase(Ease.InOutQuad);
            });
    }
}
