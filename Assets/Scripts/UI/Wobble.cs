using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wobble : MonoBehaviour
{
    public float UpDownChange = 0.5f;
    public float UpDownDuration = 4f;
    public float TiltChange = 30f;
    public float TiltDuration = 3f;

    private void Awake()
    {
        transform.DOMoveY(transform.position.y + (UpDownChange / 2f), UpDownDuration / 2f)
            .SetEase(Ease.InOutQuad)
            .OnComplete(() =>
            {
                transform.DOMoveY(transform.position.y - (UpDownChange / 2f), UpDownDuration / 2f)
                .SetLoops(-1, LoopType.Yoyo)
                .SetEase(Ease.InOutQuad);
            });

        Vector3 firstRotation = transform.eulerAngles;
        firstRotation.z += TiltChange / 2f;
        Vector3 secondRotation = transform.eulerAngles;
        secondRotation.z -= TiltChange / 2f;

        transform.DORotate(firstRotation, TiltDuration / 2f)
            .SetEase(Ease.InOutQuad)
            .OnComplete(() =>
            {
                transform.DORotate(secondRotation, TiltDuration / 2f)
                .SetLoops(-1, LoopType.Yoyo)
                .SetEase(Ease.InOutQuad);
            });
    }
}
