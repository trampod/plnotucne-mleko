using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUIManager : MonoBehaviour
{
    public HealthBar Health;
    public HealthBar Energy;

    public EntityStats Stats;

    private void Start()
    {
        RefreshUI();
    }

    public void RefreshUI()
    {
        Health.SetHealth(Stats.CurrentHealth, Stats.MaxHealth);
        Energy.SetHealth(Stats.CurrentEnergy, Stats.MaxEnergy);
    }
}
