using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class ListExtention
{
    public static TType GetRandom<TType>(this List<TType> list)
    {
        return list[Random.Range(0, list.Count)];
    }
    public static List<TType> GetRandom<TType>(this List<TType> list, int count)
    {
        return list.OrderBy(arg => Random.Range(0f,1f)).Take(count).ToList();
    }
}