using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Extention
{
    public static Vector3 xzy(this Vector3 vector)
    {
        return new Vector3(vector.x, vector.z, vector.y);
    }

    public static Vector2 xy(this Vector3 vector)
    {
        return new Vector2(vector.x, vector.y);
    }

    public static Vector2Int xy(this Vector3Int vector)
    {
        return new Vector2Int(vector.x, vector.y);
    }
}
