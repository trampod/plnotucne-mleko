using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using UnityEngine.Events;
using System;
using DG.Tweening;

public enum TeamName
{
	None,
	Player,
	GeneralEnemy,
	DestructibleObject,
	Undead,
	Goblin,
	Slime,
	Mimic,
}

public class EntityStats : MonoBehaviour
{
	[field: Separator("General")]
	[field: SerializeField]
	[field: Tooltip("Mainly for displaying. Not highly important.")]
	public string PersonalName { get; private set; }

    [field: SerializeField]
	[field: Tooltip("In most cases units don't attack or can't damage their own team.")]
	public TeamName TeamID { get; private set; }

	[field: SerializeField]
	[field: Tooltip("Damage is ignored.")]
	public bool IsImmortal { get; private set; }




	[field: Separator("Stats")]
	[field: SerializeField]
	[field: Range(1f, 200f)]
	[field: Tooltip("Maximum possible amount of health.")]
	public float MaxHealth { get; private set; } = 100f;
	[field: SerializeField]
	[field: Range(0f, 200f)]
	[field: Tooltip("Used by skills. Not needed with inanimate objects.")]
	public float MaxEnergy { get; private set; } = 0f;

	[field: SerializeField]
	[field: Range(0, 10)]
	[field: Tooltip("Represents flat damage reduction. Applied before Resistance.")]
	public int Armor { get; private set; } = 0;

	[field: SerializeField]
	[field: Range(0, 1)]
	[field: Tooltip("Represents percentage of damage that is ignored. Applied after Armor. 0=0% 1=100%")]
	public float Resistence { get; private set; } = 0f;




	[field: Separator("Runtime")]
	[field: ReadOnly]
	[field: SerializeField]
	[field: Tooltip("Current health. At start it is set to MaxHealth. Once 0 or lower causes OnDeath to trigger.")]
	public float CurrentHealth { get; private set; }

	[field: ReadOnly]
	[field: SerializeField]
	[field: Tooltip("Current energy. At start it is set to MaxEnergy. Can't be lowered bellow 0.")]
	public float CurrentEnergy { get; private set; }

	[field: ReadOnly]
	[field: SerializeField]
	[field: Tooltip("When above 0 this unit is supposed to be stunned and shouldn't move.")]
	private float StunTimer;
	public bool IsStunned => StunTimer > 0f;



	[Separator("Visuals")]
	[Tooltip("Color that flashes every time a damage is dealt to the entity.")]
	public Color DamageTint = Color.white;

	[Tooltip("For how long the damage tint lasts.")]
	public float DamageTintDuration = 0.33f;



	[Separator("Events")]
	[Tooltip("Triggers when health drops bellow 0.")]
	public UnityEvent OnDeath;

	[Tooltip("Triggers any time a damage is dealt to this entity. Can also trigger from DoT.")]
	public UnityEvent OnDamaged;

	[Tooltip("Triggers any time this entity is hit. Does not neccesitates OnDamage will trigger as well.")]
	public UnityEvent OnHit;

	[Tooltip("Triggers any time this entity's health changes. Including when it is damaged.")]
	public UnityEvent OnHealthChange;

	[Tooltip("Triggers any time this entity's energy changes.")]
	public UnityEvent OnEnergyChange;


	private int _lastEnergyGainTime;

	private void Awake()
	{
		CurrentHealth = MaxHealth;
		CurrentEnergy = MaxEnergy;
		StunTimer = 0f;
		_lastEnergyGainTime = (int)Time.fixedTime;
	}

	private void FixedUpdate()
	{
		if(StunTimer > 0f)
			StunTimer -= Time.fixedDeltaTime;

		if((int)Time.fixedTime > _lastEnergyGainTime)
        {
			AddEnergy(1);
			_lastEnergyGainTime = (int)Time.fixedTime;
        }
	}

	public void Heal(float healthAmount)
	{
		if (CurrentHealth <= 0 || healthAmount < 0)
			return;

		CurrentHealth = Mathf.Clamp(CurrentHealth + healthAmount, 0, MaxHealth);

		if (OnHealthChange != null)
			OnHealthChange.Invoke();
	}

	public void AddEnergy(float energyAmount)
    {
		CurrentEnergy = Mathf.Clamp(CurrentEnergy + energyAmount, 0, MaxEnergy);

		if (OnEnergyChange != null)
			OnEnergyChange.Invoke();
    }

	public void SetStun(float duration)
	{
		if(duration > StunTimer)
			StunTimer = duration;
	}


	public void Hit(TeamName attackerTeamID, float Damage)
	{
		if (attackerTeamID == TeamID)
			return;

		if (OnHit != null)
			OnHit.Invoke();

		ApplyDamage(Damage);
	}

	public void ApplyDamage(float damage)
	{
		if (IsImmortal)
			return;

		float finalDamage = Mathf.Clamp(damage - Armor, 0, damage);

		finalDamage *= (1 - Resistence);

		if (finalDamage > 0f)
		{
			ReduceHealth(finalDamage);

			PlayDamageTint();

			if (OnDamaged != null)
				OnDamaged.Invoke();

			if (CurrentHealth < 0)
			{
				if (OnDeath != null)
					OnDeath.Invoke();
			}
		}
	}

	private void ReduceHealth(float finalDamage)
	{
		CurrentHealth-=finalDamage;
		if (OnHealthChange != null)
			OnHealthChange.Invoke();
	}


	private bool _tiltInProgress = false;
	private void PlayDamageTint()
	{
		if (_tiltInProgress)
			return;

		var renderers = GetComponentsInChildren<SpriteRenderer>();

		Tween t = null;
		foreach (var renderer in renderers)
		{
			if (DOTween.TweensByTarget(renderer) != null)
				continue;
			renderer.DOColor(DamageTint, DamageTintDuration / 2).SetEase(Ease.Linear).SetLoops(2, LoopType.Yoyo);
		}

		if (t != null)
        {
			_tiltInProgress = true;
			t.OnComplete(() => _tiltInProgress = false);
        }
	}
}
