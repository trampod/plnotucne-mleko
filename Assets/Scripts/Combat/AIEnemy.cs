using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(EntityStats))]
public class AIEnemy : MonoBehaviour
{
	[Tooltip("Speed in units per second.")]
	public float Speed = 2f;

	[Tooltip("Weapon of the enemy. Must be set.")]
	public WeaponComponent Weapon;

	[MinMaxRange(0,5)]
	[Tooltip("In what distance range will the enemy use his weapon.")]
	public RangedFloat AttackDistance = new RangedFloat(1,2);

	[Tooltip("Animator fot the visuals. Must be set.")]
	public Animator EnemyAnimator;



	private Vector2 moveDirection = Vector2.zero;
	private Rigidbody2D _rigidbody;
	private EntityStats _entityStats;
	private Sensor _sensor;
	private Transform _currentTarget = null;
	private Vector3 _targetPosition;
	private Vector3 _basePosition;

	private void Awake()
	{
		_basePosition = transform.position;
		_targetPosition = transform.position;

		_rigidbody = GetComponent<Rigidbody2D>();
		_entityStats = GetComponent<EntityStats>();
		_sensor = GetComponentInChildren<Sensor>();

		_sensor.ObservedNew.AddListener(RegisterTarget);
		_sensor.ObservedLost.AddListener(ForgetTarget);
		_sensor.ObservedContinue.AddListener(RegisterTarget);
	}

    private void Update()
    {
		if(_currentTarget != null)
        {
			_targetPosition = _currentTarget.position;
			if (!Weapon.IsOnCooldown && (transform.position - _targetPosition).magnitude < AttackDistance.Max)
            {
				Weapon.Attack(_targetPosition);
				EnemyAnimator.SetTrigger("AttackTrigger");
            }
        }

		Vector3 positionDifference = _targetPosition - transform.position;

		if ((_currentTarget == null && positionDifference.magnitude < 0.1) || (_currentTarget != null && positionDifference.magnitude < AttackDistance.Min))
			_rigidbody.velocity = Vector2.zero;
		else
			_rigidbody.velocity = positionDifference.normalized * Speed;

		EnemyAnimator.SetBool("IsWalking", _rigidbody.velocity.magnitude > 0.2);
		EnemyAnimator.transform.up = (Vector2)_rigidbody.velocity.normalized;
    }

    private void RegisterTarget(GameObject observed)
    {
		if (_currentTarget != null)
			return;

		if(observed.TryGetComponent(out EntityStats stats))
        {
			if(stats.TeamID == TeamName.Player)
            {
				_currentTarget = observed.transform;
            }
        }
    }

	private void ForgetTarget(GameObject observed)
    {
		if(_currentTarget == observed.transform)
        {
			_currentTarget = null;
        }
    }
}
