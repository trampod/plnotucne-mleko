using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D))]
public class TriggerColliderSensor : Sensor
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        AddNewObserved(collision.gameObject);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        RemoveObserved(collision.gameObject);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        UpdateObserved(collision.gameObject);
    }
}
