using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Failed attempt, maybe will be fixed in the future
[RequireComponent(typeof(CircleCollider2D))]
public class CircularTriggerRaycastSensor : Sensor
{
    public LayerMask mask;

    CircleCollider2D _collider;

    private void Awake()
    {
        _collider = GetComponent<CircleCollider2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(RaycastCheck(_collider))
            AddNewObserved(collision.gameObject);
        else if(_observedGameObjects.Contains(collision.gameObject))
            RemoveObserved(collision.gameObject);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        RemoveObserved(collision.gameObject);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(RaycastCheck(_collider))
            UpdateObserved(collision.gameObject);
        else if(_observedGameObjects.Contains(collision.gameObject))
            RemoveObserved(collision.gameObject);
    }

    private bool RaycastCheck(Collider2D collision)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, collision.transform.position - transform.position, _collider.radius * transform.lossyScale.magnitude, mask, -10, 10); ;
        Debug.DrawLine(transform.position, (Vector3)hit.point, Color.magenta);
        Debug.DrawLine(transform.position, collision.transform.position, Color.red);

        return hit.collider != null && hit.collider.gameObject == collision.gameObject;
    }
}
