using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class Sensor : MonoBehaviour
{
    [Tooltip("Triggers when new object is observed.")]
    public UnityEvent<GameObject> ObservedNew;
    [Tooltip("Triggers when object continued to be observed.")]
    public UnityEvent<GameObject> ObservedLost;
    [Tooltip("Triggers when object is no longer observed.")]
    public UnityEvent<GameObject> ObservedContinue;

    protected HashSet<GameObject> _observedGameObjects = new HashSet<GameObject>();

    public IReadOnlyCollection<GameObject> ObservedGameObjects => _observedGameObjects;


    protected void AddNewObserved(GameObject observedGameObject)
    {
        _observedGameObjects.Add(observedGameObject);
        if (ObservedNew != null)
            ObservedNew.Invoke(observedGameObject);
    }

    protected void UpdateObserved(GameObject observedGameObject)
    {
        if (!_observedGameObjects.Contains(observedGameObject))
        {
            _observedGameObjects.Add(observedGameObject);
            if (ObservedNew != null)
                ObservedNew.Invoke(observedGameObject);
        }
        else
        {
            if (ObservedContinue != null)
                ObservedContinue.Invoke(observedGameObject);
        }
    }

    protected void RemoveObserved(GameObject observedGameObject)
    {
        _observedGameObjects.Remove(observedGameObject);
        if (ObservedLost != null)
            ObservedLost.Invoke(observedGameObject);
    }
}
