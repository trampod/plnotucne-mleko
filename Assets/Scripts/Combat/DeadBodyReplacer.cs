using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadBodyReplacer : MonoBehaviour
{
    [Tooltip("Upon replacing this object will be spawned while the original is destroyed.")]
    public GameObject DeadBodySprite;

    public void Replace()
    {
        Instantiate(DeadBodySprite, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
