using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(EntityStats))]
public class PlayerScript : MonoBehaviour
{
	[Tooltip("Speed in units per second.")]
	public float Speed = 1f;


	public Animator CharacterAnimator;

	public WeaponComponent MainWeapon => Weapons[CurrentWeapon];

	[field: Tooltip("Index of currently selected weapon")]
	[field: ReadOnly]
	public int CurrentWeapon { get; private set; } = 0;
	[Tooltip("List of possible weapons. Needs to match the list of weapon icons.")]
	public List<WeaponComponent> Weapons;

	[Separator("Events")]
	[Tooltip("Triggers every time a weapon is changed. Gives index of the new weapon.")]
	public UnityEvent<int> WeaponChanged;


    private Vector2 moveDirection = Vector2.zero;
	private Vector2 aimVector;

	private Rigidbody2D _rigidbody;

	private EntityStats _entityStats;

	[SerializeField]
	[Tooltip("Interactor for activating objects on the ground.")]
	private Interactor _interactor;

	private void Awake()
	{
		_rigidbody = GetComponent<Rigidbody2D>();
		_entityStats = GetComponent<EntityStats>();

		if (WeaponChanged != null)
			WeaponChanged.Invoke(CurrentWeapon);
	}

	private void Update()
	{
		if (_entityStats.IsStunned)
			return;

		CharacterAnimator.SetBool("IsWalking",moveDirection.magnitude > 0);

		_rigidbody.velocity = moveDirection * Speed;
	}



	public void OnMoveInput(InputAction.CallbackContext context)
	{
		//Debug.Log("move");
		moveDirection = context.ReadValue<Vector2>().normalized;
	}

	public void OnLookInput(InputAction.CallbackContext context)
	{
		//Debug.Log("look");
	}

	public void OnDirectLookInput(InputAction.CallbackContext context)
	{
		//Debug.Log("direct-look");
		aimVector = context.ReadValue<Vector2>();
		var target = Camera.main.ScreenToWorldPoint(aimVector);
		if (MainWeapon != null && !_entityStats.IsStunned)
			MainWeapon.SetAim(target);

		CharacterAnimator.transform.up = ((Vector2)target - (Vector2)transform.position).normalized;
	}

	public void OnInteractionInput(InputAction.CallbackContext context)
	{
		if (!context.performed)
			return;

		Debug.Log("interact");
		if (_interactor.ClosestInteractable != null)
        {
			_interactor.ClosestInteractable.Activate(_entityStats.gameObject);
        }
	}

	public void OnFireInput(InputAction.CallbackContext context)
	{
		if(!context.performed)
			return;
		//Debug.Log("fire");
		var target = Camera.main.ScreenToWorldPoint(aimVector);
		if (MainWeapon != null && MainWeapon.CanAttack)
        {
			MainWeapon.Attack(target);
			CharacterAnimator.SetTrigger("AttackTrigger");
        }
	}

	public void OnWeaponSwitch(InputAction.CallbackContext context)
	{
		if (!context.performed)
			return;

		CurrentWeapon = (CurrentWeapon + 1) % Weapons.Count;

		if (WeaponChanged != null)
			WeaponChanged.Invoke(CurrentWeapon);
	}
}
