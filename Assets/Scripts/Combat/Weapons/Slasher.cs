using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(Collider2D))]
public class Slasher : WeaponComponent
{
	[field: Separator("Slasheer Specific")]
	[field: SerializeField]
	[field: Tooltip("Sprite renderer that is turned on durring attack.")]
	public SpriteRenderer SlashGraphics;

	[field: SerializeField]
	[field: Tooltip("How long does the slash lasts.")]
	public float SlashDuration { get; private set; }

	[field: SerializeField]
	[field: Tooltip("Strength of the knockback of the attack.")]
	public float SlashKnockback { get; private set; }

	[field: SerializeField]
	[field: Tooltip("For how long is the target stunned")]
	public float SlashStunDuration { get; private set; }

	[SerializeField]
	[ReadOnly]
	[Tooltip("Attack is in progress.")]
	private bool isSlashing;

	private Collider2D triggerCollider;
	private HashSet<EntityStats> alreadyHit = new HashSet<EntityStats>();

	protected override void Awake()
	{
		base.Awake();
		triggerCollider = GetComponent<Collider2D>();
		triggerCollider.isTrigger = true;
		triggerCollider.enabled = true;
		SlashGraphics.enabled = false;
	}

	private void OnTriggerStay2D(Collider2D other)
	{
		if (!isSlashing)
			return;

		if(other.TryGetComponent<EntityStats>(out EntityStats stats))
		{
			if (alreadyHit.Contains(stats))
				return;
			//Debug.Log($"{name}.Hit({other.name})");
			alreadyHit.Add(stats);

			stats.Hit(ownerStats.TeamID, Damage);
			stats.SetStun(SlashStunDuration);

			if(other.TryGetComponent<Rigidbody2D>(out Rigidbody2D rigidbody))
			{
				Vector2 force = other.transform.position - transform.position;
				force.Normalize();
				force *= SlashKnockback;
				rigidbody.AddForce(force);
			}

		}
	}

	public override bool TryAttack()
	{
		if (isSlashing)
			return false;
		//Debug.Log($"{name}.Attack()");

		isSlashing = true;
		SlashGraphics.enabled = true;
		alreadyHit.Clear();

		Invoke(nameof(TurnOffAttack), SlashDuration);

		SetCooldown();

		return true;
	}

	private void TurnOffAttack()
	{
		isSlashing = false;
		SlashGraphics.enabled = false;
	}

	public override void SetAim(Vector2 target)
	{
		Vector2 thisPosition2 = this.transform.position;
		transform.up = (target - thisPosition2).normalized;
	}
}
