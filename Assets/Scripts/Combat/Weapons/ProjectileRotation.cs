using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileRotation : MonoBehaviour
{
    [SerializeField]
    private Rigidbody2D projectileRigidbody;

	private void LateUpdate()
	{
		if(projectileRigidbody != null)
		{
			Vector3 velocity = projectileRigidbody.velocity;
			if(velocity.magnitude > 0)
				transform.up = velocity;
		}
	}
}
