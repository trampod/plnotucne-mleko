using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WeaponComponent : MonoBehaviour
{
	[field: Separator("Shared")]
	[field: SerializeField]
	[field: Range(0,100)]
	[field: Tooltip("How much damage attack does. How it is applied is specific for each weapon.")]
	public float Damage { get; private set; }

	[field: SerializeField]
	[field: Range(0,100)]
	[field: Tooltip("How long before the weapon can be used again. Measured in seconds.")]
	public float Cooldown { get; private set; }

	[SerializeField]
	[ReadOnly]
	[Tooltip("Runtime value of active cooldown.")]
	private float cooldownTimer;

	public float EnergyCost = 10f;

	public bool IsOnCooldown => cooldownTimer > 0;

	public bool CanAttack => !IsOnCooldown && ownerStats.CurrentEnergy > EnergyCost;

	protected EntityStats ownerStats;

	protected virtual void Awake()
	{
		RenewOwnership();
	}

	protected virtual void Update()
	{
		cooldownTimer -= Time.deltaTime;
	}

	public abstract void SetAim(Vector2 target);

    public abstract bool TryAttack();

    public void Attack(Vector2 target)
	{
		SetAim(target);
		if(TryAttack())
			ownerStats.AddEnergy(-EnergyCost);
	}

	public void SetCooldown()
	{
		cooldownTimer = Cooldown;
	}

	public void RenewOwnership()
	{
		ownerStats = GetComponentInParent<EntityStats>();
	}
}
