using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileShooter : WeaponComponent
{
	[field: Separator("Projectile Specific")]
	[field: SerializeField]
	[field: Tooltip("Gameobject that is spawned when attacked.")]
	public GameObject ProjectilePrefab;

	[field: SerializeField]
	[field: Tooltip("Speed at which the projectile is shot.")]
	public float ProjectileSpeed { get; private set; }

	[field: SerializeField]
	[field: Tooltip("After what time does the projectile disappear.")]
	public float ProjectileDuration { get; private set; }

	[SerializeField]
	[ReadOnly]
	private Vector2 aimDirection;



	public override void SetAim(Vector2 target)
	{
		Vector2 thisPosition2 = this.transform.position;
		aimDirection = (target - thisPosition2).normalized;
	}

	public override bool TryAttack()
	{

		GameObject projectile = Instantiate(ProjectilePrefab, transform.position,transform.rotation);

		var projectileCollider = projectile.GetComponent<Collider2D>();

		var playerCollider = GetComponentInParent<Collider2D>();

		if(projectileCollider != null && playerCollider != null)
		{
			Physics2D.IgnoreCollision(playerCollider,projectileCollider);
		}

		var projectileRigidbody = projectile.GetComponent<Rigidbody2D>();

		if(projectileRigidbody != null)
		{
			projectileRigidbody.velocity = aimDirection * ProjectileSpeed;
			projectile.AddComponent<TimeToLive>().StartCountdown(ProjectileDuration);

			ProjectileDamage damage = projectile.GetComponent<ProjectileDamage>();
			if (damage == null)
				damage = projectile.AddComponent<ProjectileDamage>();

			damage.Damage = Damage;
			damage.TeamID = ownerStats.TeamID;
		}
		else
		{
			Destroy(projectile);
		}

		SetCooldown();

		return true;
	}
}
