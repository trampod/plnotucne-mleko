using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D))]
public class ProjectileDamage : MonoBehaviour
{
	[ReadOnly]
	[Tooltip("Team who shot the projectile.")]
    public TeamName TeamID;
	[ReadOnly]
	[Tooltip("Damage that will be dealt.")]
    public float Damage;

	[Tooltip("Triggers on every hit, except the final.")]
	public UnityEvent OnHit;

	[Tooltip("Triggers on the last hit, that deals damage.")]
	public UnityEvent OnDamage;

	private void Awake()
	{
		var collider = GetComponent<Collider2D>();
		collider.enabled = true;
		collider.isTrigger = false;
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.collider.TryGetComponent<EntityStats>(out EntityStats stats))
		{
			if (stats.TeamID == TeamID)
            {
				if (OnHit != null)
					OnHit.Invoke();
				return;
            }

			if(OnDamage != null)
				OnDamage.Invoke();

			stats.Hit(TeamID, Damage);
			Destroy(gameObject);
		}
		else
        {
			if (OnHit != null)
				OnHit.Invoke();
        }
	}
}
