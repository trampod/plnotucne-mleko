using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MilkItem : Interactable
{
    private void Awake()
    {
        MilkManager.Instance.RegisterMilk();
    }

    public override void Activate(GameObject activator)
    {
        MilkManager.Instance.GrabMilk();

        GetComponentInChildren<SpriteRenderer>().DOFade(0f, 0.3f).OnComplete(() => Destroy(gameObject));
        activator.GetComponentInChildren<Interactor>().UnregisterInteractable(gameObject);
        Destroy(this);

        base.Activate(activator);
    }
}
