using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class MilkManager : UnityEngine.Singleton<MilkManager>
{
    [field: SerializeField]
    [field: ReadOnly]
    public int GrabbedMilks { get; private set; }
    
    [field: SerializeField]
    [field: ReadOnly]
    public int MilkToGrab { get; private set; }

    public UnityEvent MilkGrabbed;

    public UnityEvent MilkCollected;

    public void Reset()
    {
        GrabbedMilks = 0;
        MilkToGrab = 0;
    }

    public void RegisterMilk()
    {
        MilkToGrab++;
    }

    public void GrabMilk()
    {
        GrabbedMilks++;

        if (MilkGrabbed != null)
            MilkGrabbed.Invoke();

        if(GrabbedMilks == MilkToGrab)
        {
            FinishGame();
        }
    }

    private void FinishGame()
    {
        PlayerScript player = FindObjectOfType<PlayerScript>();
        if(player != null)
        {
            PlayerInput playerInput = player.GetComponent<PlayerInput>();
            Destroy(playerInput);
            Destroy(player);
        }

        if (MilkCollected != null)
            MilkCollected.Invoke();
    }
}
