using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosives : Interactable
{
    [Tooltip("Damage dealt to EVERYONE in the Radius.")]
    public float Damage;

    [Tooltip("How long before the explosion is triggered.")]
    public float Delay;

    [Tooltip("How much are the hit objects thrown away from the epicenter.")]
    public float KnokbackForce;

    [Tooltip("Size of explosion. Affects both the blast size and where the damage is applied.")]
    public float Radius;

    [Tooltip("Gameobject for explosion. Needs to be sprite with radius of 1 unit. Needs to be set.")]
    public GameObject ExplosionEffect;

    [Tooltip("Animator of the explosives. Needs to be set.")]
    public Animator ExplosivesAnimator;


    public override void Activate(GameObject activator)
    {
        if(!WasActivated)
        {
            ExplosivesAnimator.SetTrigger("ActivateTrigger");
            Invoke(nameof(Detonate),Delay);
        }

        base.Activate(activator);
    }

    private void Detonate()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, Radius);

        foreach(var collider in colliders)
        {
            if(collider.TryGetComponent<EntityStats>(out EntityStats stats))
            {
                stats.ApplyDamage(Damage);

                if(stats.TryGetComponent<Rigidbody2D>(out Rigidbody2D rigidbody))
                {
                    rigidbody.AddForce((rigidbody.transform.position - transform.position).normalized * KnokbackForce);
                }
            }
        }


        GameObject explosion = Instantiate(ExplosionEffect, transform.position, Quaternion.identity);
        explosion.transform.localScale = Vector3.zero;
        
        Sequence seq = DOTween.Sequence();

        seq.Append(explosion.transform.DOScale(Radius, 0.15f));

        seq.Append(explosion.transform.DOScale(Radius * 1.25f, 0.2f));
        
        SpriteRenderer renderer = explosion.GetComponent<SpriteRenderer>();
        if(renderer != null)
        {
            seq.Join(renderer.DOFade(0,0.2f));
        }

        seq.OnComplete(()=> { Destroy(explosion); Destroy(gameObject); });
    }
}
