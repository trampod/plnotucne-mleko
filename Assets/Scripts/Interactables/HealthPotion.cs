using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPotion : Interactable
{
    public float HealthAmount;

    public SpriteRenderer PotionVisuals;

    public override void Activate(GameObject activator)
    {
        if(activator.TryGetComponent<EntityStats>(out EntityStats stats))
        {
            float prevHealth = stats.CurrentHealth;
            stats.Heal(HealthAmount);
            if (prevHealth != stats.CurrentHealth)
            {
                PotionVisuals.DOFade(0f,0.3f).OnComplete(() => Destroy(gameObject));
                activator.GetComponentInChildren<Interactor>().UnregisterInteractable(gameObject);
                Destroy(this);
            }
        }

        base.Activate(activator);
    }
}
