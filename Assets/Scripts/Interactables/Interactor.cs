using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactor : MonoBehaviour
{
    [ReadOnly]
    [SerializeField]
    [Tooltip("All Interactables in the area.")]
    private List<Interactable> _interactables = new List<Interactable>();

    [field: SerializeField]
    [field: ReadOnly]
    [field: Tooltip("The closest Interactable object.")]
    public Interactable ClosestInteractable { get; private set; } = null;

    [field: SerializeField]
    [field: ReadOnly]
    [field: Tooltip("Buttton press UI.")]
    private RectTransform UIElement;

    private Camera _camera;

    private void Awake()
    {
        _camera = Camera.main;
    }

    public void Update()
    {
        UpdateUI();
        UpdateClosest();
    }

    private void UpdateUI()
    {
        if(UIElement != null)
        {
            if(ClosestInteractable == null)
            {
                UIElement.gameObject.SetActive(false);
                return;
            }

            UIElement.gameObject.SetActive(true);

            Vector3 screenPos = _camera.WorldToScreenPoint(ClosestInteractable.transform.position + Vector3.up * 0.5f);

            UIElement.position = screenPos;
        }
    }

    private void UpdateClosest()
    {
        float distance = float.MaxValue;
        ClosestInteractable = null;

        foreach (var interactable in _interactables)
        {
            float tmpDist = Vector3.Distance(interactable.gameObject.transform.position, transform.position);
            if(tmpDist < distance)
            {
                distance = tmpDist;
                ClosestInteractable = interactable;
            }
        }
    }

    public void RegisterInteractable(GameObject gameObject)
    {
        if(gameObject.TryGetComponent<Interactable>(out Interactable interactable))
        {
            if(!_interactables.Contains(interactable))
            {
                _interactables.Add(interactable);
            }
            UpdateClosest();
        }
    }

    public void UnregisterInteractable(GameObject gameObject)
    {
        if(gameObject.TryGetComponent<Interactable>(out Interactable interactable))
        {
            if(_interactables.Contains(interactable))
            {
                _interactables.Remove(interactable);
                UpdateClosest();
            }
        }
    }
}
