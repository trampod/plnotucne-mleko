using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public abstract class Interactable : MonoBehaviour
{
    [Tooltip("If the object was already interacted with.")]
    public bool WasActivated { get; protected set; }

    public virtual void Activate(GameObject activator)
    {
        WasActivated = true;
    }
}
