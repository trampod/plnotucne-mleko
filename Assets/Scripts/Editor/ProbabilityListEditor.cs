using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof(ProbabilityList))]
public class ProbabilityListEditor : Editor
{
    ProbabilityList propList;
    //ReorderableList objectList;


    private void OnEnable()
    {
        propList = (ProbabilityList)target;
        //objectList = new ReorderableList(serializedObject, serializedObject.FindProperty("PropProbabilityList"));
    }

    public override void OnInspectorGUI()
    {
        DrawScriptField();

        if (propList == null)
            return;

        GUILayout.BeginHorizontal();

        GUILayout.Label("List Name");

        propList.ListName = GUILayout.TextField(propList.ListName);

        GUILayout.EndHorizontal();

        //serializedObject.Update();

        float sum = 0f;
        for (int i = 0; i < propList.ProbabilityItems.Count; i++)
        {
            GUILayout.BeginHorizontal();
            ProbabilityElement item = propList.ProbabilityItems[i];
            item.gameObject = (GameObject)EditorGUILayout.ObjectField(item.gameObject,typeof(GameObject),false,GUILayout.ExpandWidth(false));

            item.weight = GUILayout.HorizontalSlider(item.weight,0f,1f);
            //EditorGUILayout.ObjectField(serializedObject.FindProperty("PropProbabilityList").GetArrayElementAtIndex(i));

            GUILayout.Label($"{(item.chance*100).ToString("##0.")}%");

            if (GUILayout.Button("-", GUILayout.ExpandWidth(false)))
            {
                propList.ProbabilityItems.RemoveAt(i);
                i--;
            }
            else
            {
                sum += item.weight;
            }
            

            GUILayout.EndHorizontal();
        }

        foreach(var item in propList.ProbabilityItems)
        {
            item.chance = item.weight / sum;
        }

        ReorderableList list = new ReorderableList(serializedObject,serializedObject.FindProperty("PropProbabilityList"));
        

        GUILayout.BeginHorizontal();
        if(GUILayout.Button("+",GUILayout.ExpandWidth(false)))
        {
            propList.ProbabilityItems.Add(new ProbabilityElement(null, 1f));
        }
        GUILayout.EndHorizontal();

        if (GUI.changed)
        {
            EditorUtility.SetDirty(target);
        }
    }
    private void DrawScriptField()
    {
        // Disable editing
        EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.ObjectField("Script", MonoScript.FromScriptableObject((ProbabilityList)target), typeof(ProbabilityList), false);
        EditorGUI.EndDisabledGroup();
    }
}
