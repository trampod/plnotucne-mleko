using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(RoomDefinition))]
public class RoomDefinitionEditor : Editor
{
    RoomDefinition room;
    //SerializedProperty size;
    //SerializedProperty array;

    int tabIndex = 0;

    GUIStyle wallStyle;
    GUIStyle floorStyle;
    GUIStyle doorStyle;
    GUIStyle hallStyle;
    GUIStyle emptyStyle;

    int cellSize = 30;

    private void OnEnable()
    {
        room = (RoomDefinition)target;
        //size = serializedObject.FindProperty("RoomSize");
        //array = serializedObject.FindProperty("RoomTiles");
    }

    private void StyleSetup()
    {

        wallStyle = new GUIStyle(GUI.skin.button);
        wallStyle.margin = new RectOffset(1, 1, 1, 1);
        wallStyle.padding = new RectOffset(0, 0, 0, 0);
        wallStyle.normal.background = wallStyle.hover.background = wallStyle.active.background = wallStyle.focused.background = Resources.Load("wall_preview") as Texture2D;
        hallStyle = new GUIStyle(wallStyle);
        hallStyle.normal.background = hallStyle.hover.background = hallStyle.active.background = hallStyle.focused.background = Resources.Load("hall_preview") as Texture2D;
        doorStyle = new GUIStyle(wallStyle);
        doorStyle.normal.background = doorStyle.hover.background = doorStyle.active.background = doorStyle.focused.background = Resources.Load("door_preview") as Texture2D;
        floorStyle = new GUIStyle(wallStyle);
        floorStyle.normal.background = floorStyle.hover.background = floorStyle.active.background = floorStyle.focused.background = Resources.Load("floor_preview") as Texture2D;
        emptyStyle = new GUIStyle(wallStyle);
        emptyStyle.normal.background = emptyStyle.hover.background = emptyStyle.active.background = emptyStyle.focused.background = Resources.Load("empty_preview") as Texture2D;

    }

    public override void OnInspectorGUI()
    {
        StyleSetup();



        EditorGUILayout.BeginVertical();

        //base.OnInspectorGUI();

        DrawScriptField();
        //EditorGUILayout.PropertyField(size);
        //serializedObject.ApplyModifiedProperties();

        room.RoomSize = EditorGUILayout.Vector2IntField("Room Size", room.RoomSize);


        //Vector2Int vector = size.vector2IntValue;

        if (room.RoomSize.x < 1)
            room.RoomSize.x = 1;
        if (room.RoomSize.y < 1)
            room.RoomSize.y = 1;

        //size.vector2IntValue = vector;
        //serializedObject.ApplyModifiedProperties();

        //Undo.RecordObject(target,"map changes");
        

        room.RealRoomSize.x = room.RoomSize.x + 2;
        room.RealRoomSize.y = room.RoomSize.y + 2;

        if (room.prevSize.x != room.RealRoomSize.x || room.prevSize.y != room.RealRoomSize.y)
        {
            room.ResizeArray();
        }

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("Prop List");
        room.PropList = (ProbabilityList)EditorGUILayout.ObjectField(room.PropList, typeof(ProbabilityList), false);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();

        GUIStyle tabStyle = new GUIStyle(GUI.skin.button);

        tabStyle.margin.left = 0;
        tabStyle.margin.right = 0;
        tabStyle.padding.top = 5;
        tabStyle.padding.bottom = 5;

        GUIStyle tabStyleSelected = new GUIStyle(tabStyle);
        tabStyleSelected.normal.textColor = new Color(0.6f, 0.6f, 1);
        tabStyleSelected.hover.textColor = new Color(0.6f, 0.6f, 1);
        tabStyleSelected.focused.textColor = new Color(0.6f, 0.6f, 1);
        tabStyleSelected.active.textColor = new Color(0.6f, 0.6f, 1);

        if (GUILayout.Button("Floorplan", tabIndex == 0 ? tabStyleSelected : tabStyle))
        {
            tabIndex = 0;
        }
        if (GUILayout.Button("Doors", tabIndex == 1 ? tabStyleSelected : tabStyle))
        {
            tabIndex = 1;
        }
        if (GUILayout.Button("Props", tabIndex == 2 ? tabStyleSelected : tabStyle))
        {
            tabIndex = 2;
        }
        if (GUILayout.Button("Enemies", tabIndex == 3 ? tabStyleSelected : tabStyle))
        {
            tabIndex = 3;
        }

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();


        EditorGUILayout.BeginScrollView(new Vector2(200,200));

        if (room == null)
            return;

        switch(tabIndex)
        {
            case 0:
                DisplayFloorTab();
                break;
            case 1:
                DisplayDoorTab();
                break;
            case 2:
                DisplayPropTab();
                break;
            case 3:
                DisplayEnemyTab();
                break;
            default:
                break;
        }

        EditorGUILayout.EndScrollView();

        EditorGUILayout.EndVertical();

        //serializedObject.ApplyModifiedProperties();
        if (GUI.changed)
        {
            EditorUtility.SetDirty(target);
            Debug.Log("GUI.changed = true");
            Debug.Log(target == room);
        }
    }

    void DisplayFloorTab()
    {

        //bool[,] array = new bool[room.RealRoomSize.x, room.RealRoomSize.y];

        //for (int x = 0; x < room.RealRoomSize.x; x++)
        //{
        //    for (int y = 0; y < room.RealRoomSize.y; y++)
        //    {
        //        array[x, y] = room.GetTile(x, y) == RoomTile.Floor;
        //    }
        //}

        GUIStyle style = new GUIStyle();
        //style.clipping = TextClipping.Overflow;
        //style.alignment = TextAnchor.UpperLeft;
        style.stretchHeight = false;
        style.stretchWidth = false;


        EditorGUILayout.BeginVertical(style);
        for (int y = 0; y < room.RealRoomSize.y; y++)
        {
            EditorGUILayout.BeginHorizontal(style);//, GUILayout.Width(room.RealRoomSize.y * 20));
            for (int x = 0; x < room.RealRoomSize.x; x++)
            {
                Vector2Int v = new Vector2Int(x, y);
                RoomTile roomTile = room.GetTile(x, y);
                EditorGUI.BeginDisabledGroup(isBorder(x, y));
                if (GUILayout.Button("", getStyle(x,y), GUILayout.Width(cellSize), GUILayout.Height(cellSize)))
                {
                    if(roomTile == RoomTile.Floor)
                    {
                        room.UnSetFloor(x,y);
                    }
                    else
                    {
                        room.SetFloor(x,y);
                    }
                    //array[x, y] = !array[x, y];
                }
                if (room.EnemyPositions.Contains(v))
                {
                    //fukken magic numbers
                    GUI.DrawTexture(new Rect(x * (cellSize + 1) + 1, y * (cellSize + 2) + 1, cellSize, cellSize + 3), Resources.Load("enemy_preview") as Texture2D);
                }
                if (room.PropPositions.Contains(v))
                {
                    //fukken magic numbers
                    GUI.DrawTexture(new Rect(x * (cellSize + 1) + 1, y * (cellSize + 2) + 1, cellSize, cellSize + 3), Resources.Load("prop_preview") as Texture2D);
                }
                EditorGUI.EndDisabledGroup();
            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();
        //EditorGUI.DrawRect(new Rect(1, 2, cellSize, cellSize),Color.red);

        //for (int x = 0; x < room.RealRoomSize.x; x++)
        //{
        //    for (int y = 0; y < room.RealRoomSize.y; y++)
        //    {
        //        room.SetTile(x, y, array[x, y] ? RoomTile.Floor : RoomTile.Empty);
        //    }
        //}



        #region HelperFunctions

        bool isBorder(int x, int y)
        {
            return x == 0 || y == 0 || x == room.RealRoomSize.x - 1 || y == room.RealRoomSize.y - 1;
        }

        #endregion
    }

    void DisplayDoorTab()
    {

        GUIStyle style = new GUIStyle();
        style.stretchHeight = false;
        style.stretchWidth = false;


        EditorGUILayout.BeginVertical(style);
        for (int y = 0; y < room.RealRoomSize.y; y++)
        {
            EditorGUILayout.BeginHorizontal(style);//, GUILayout.Width(room.RealRoomSize.y * 20));
            for (int x = 0; x < room.RealRoomSize.x; x++)
            {
                RoomTile roomTile = room.GetTile(x, y);
                Vector2Int v = new Vector2Int(x, y);
                EditorGUI.BeginDisabledGroup(roomTile != RoomTile.Wall);
                if (GUILayout.Button("", getStyle(v), GUILayout.Width(cellSize), GUILayout.Height(cellSize)))
                {
                    if (room.DoorPositions.Contains(v))
                    {
                        room.DoorPositions.Remove(v);
                    }
                    else
                    {
                        if(room.CanPlaceDoor(x,y))
                        {
                            room.DoorPositions.Add(v);
                            break;
                        }
                    }
                }
                if (room.EnemyPositions.Contains(v))
                {
                    //fukken magic numbers
                    GUI.DrawTexture(new Rect(x * (cellSize + 1) + 1, y * (cellSize + 2) + 1, cellSize, cellSize + 3), Resources.Load("enemy_preview") as Texture2D, ScaleMode.ScaleToFit, true, 0, Color.gray, 0, 0);
                }
                if (room.PropPositions.Contains(v))
                {
                    //fukken magic numbers
                    GUI.DrawTexture(new Rect(x * (cellSize + 1) + 1, y * (cellSize + 2) + 1, cellSize, cellSize + 3), Resources.Load("prop_preview") as Texture2D, ScaleMode.ScaleToFit, true, 0, Color.gray, 0, 0);
                }
                EditorGUI.EndDisabledGroup();
            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();
    }

    void DisplayPropTab()
    {

        GUIStyle style = new GUIStyle();
        style.stretchHeight = false;
        style.stretchWidth = false;

        EditorGUILayout.BeginVertical(style);
        for (int y = 0; y < room.RealRoomSize.y; y++)
        {
            EditorGUILayout.BeginHorizontal(style);//, GUILayout.Width(room.RealRoomSize.y * 20));
            for (int x = 0; x < room.RealRoomSize.x; x++)
            {
                RoomTile roomTile = room.GetTile(x, y);
                Vector2Int v = new Vector2Int(x, y);
                EditorGUI.BeginDisabledGroup(roomTile != RoomTile.Floor || room.EnemyPositions.Contains(v));
                if (GUILayout.Button("", getStyle(v), GUILayout.Width(cellSize), GUILayout.Height(cellSize)))
                {
                    if (room.PropPositions.Contains(v))
                    {
                        room.PropPositions.Remove(v);
                    }
                    else
                    {
                        room.PropPositions.Add(v);
                    }
                }
                if(room.PropPositions.Contains(v))
                {
                    //fukken magic numbers
                    GUI.DrawTexture(new Rect(x * (cellSize + 1) + 1, y * (cellSize + 2) + 1, cellSize, cellSize + 3), Resources.Load("prop_preview") as Texture2D);
                }
                if (room.EnemyPositions.Contains(v))
                {
                    //fukken magic numbers
                    GUI.DrawTexture(new Rect(x * (cellSize + 1) + 1, y * (cellSize + 2) + 1, cellSize, cellSize + 3), Resources.Load("enemy_preview") as Texture2D, ScaleMode.ScaleToFit, true, 0, Color.gray, 0, 0);
                }
                EditorGUI.EndDisabledGroup();
            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();
    }

    void DisplayEnemyTab()
    {

        GUIStyle style = new GUIStyle();
        style.stretchHeight = false;
        style.stretchWidth = false;

        EditorGUILayout.BeginVertical(style);
        for (int y = 0; y < room.RealRoomSize.y; y++)
        {
            EditorGUILayout.BeginHorizontal(style);//, GUILayout.Width(room.RealRoomSize.y * 20));
            for (int x = 0; x < room.RealRoomSize.x; x++)
            {
                RoomTile roomTile = room.GetTile(x, y);
                Vector2Int v = new Vector2Int(x, y);
                EditorGUI.BeginDisabledGroup(roomTile != RoomTile.Floor || room.PropPositions.Contains(v));
                if (GUILayout.Button("", getStyle(v), GUILayout.Width(cellSize), GUILayout.Height(cellSize)))
                {
                    if (room.EnemyPositions.Contains(v))
                    {
                        room.EnemyPositions.Remove(v);
                    }
                    else
                    {
                        room.EnemyPositions.Add(v);
                    }
                }
                if (room.EnemyPositions.Contains(v))
                {
                    //fukken magic numbers
                    GUI.DrawTexture(new Rect(x * (cellSize + 1) + 1, y * (cellSize + 2) + 1, cellSize, cellSize + 3), Resources.Load("enemy_preview") as Texture2D);
                }
                if (room.PropPositions.Contains(v))
                {
                    //fukken magic numbers
                    GUI.DrawTexture(new Rect(x * (cellSize + 1) + 1, y * (cellSize + 2) + 1, cellSize, cellSize + 3), Resources.Load("prop_preview") as Texture2D, ScaleMode.ScaleToFit, true, 0, Color.gray, 0, 0);
                }
                EditorGUI.EndDisabledGroup();
            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();
    }
    private void DrawScriptField()
    {
        // Disable editing
        EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.ObjectField("Script", MonoScript.FromScriptableObject((RoomDefinition)target), typeof(RoomDefinition), false);
        EditorGUI.EndDisabledGroup();
    }

    GUIStyle getStyle(RoomTile roomTile)
    {
        switch (roomTile)
        {
            case RoomTile.Empty:
                return emptyStyle;
            case RoomTile.Floor:
                return floorStyle;
            case RoomTile.Wall:
                return wallStyle;
            default:
                return emptyStyle;
        }
    }

    GUIStyle getStyle(Vector2Int v)
    {
        RoomTile roomTile = room.GetTile(v.x, v.y);
                if (room.DoorPositions.Contains(v))
                    return doorStyle;

        switch (roomTile)
        {
            case RoomTile.Empty:
                return emptyStyle;
            case RoomTile.Floor:
                return floorStyle;
            case RoomTile.Wall:
                return wallStyle;
            default:
                return emptyStyle;
        }
    }

    GUIStyle getStyle(int x, int y)
    {
        return getStyle(new Vector2Int(x, y));
    }

    void Toggle2DArray(ref bool[,] array)
    {
        GUIStyle style = new GUIStyle();
        //style.clipping = TextClipping.Overflow;
        //style.alignment = TextAnchor.UpperLeft;
        style.stretchHeight = false;
        style.stretchWidth = false;
        

        EditorGUILayout.BeginVertical(style);
        for (int x = 0; x < array.GetLength(0); x++)
        {
            EditorGUILayout.BeginHorizontal(style, GUILayout.Width(array.GetLength(1)*20));
            for (int y = 0; y < array.GetLength(1); y++)
            {
                array[x,y] = EditorGUILayout.Toggle(array[x,y]);
            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();
    }
}
