using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomInitialRotation : MonoBehaviour
{
    private void Awake()
    {
        transform.eulerAngles = new Vector3(0, 0, Random.Range(0, 360));
        Destroy(this);
    }
}
