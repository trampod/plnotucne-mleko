using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRandom : MonoBehaviour
{
    [Tooltip("During Awake it will spawn one of these GameObjects.")]
    public List<GameObject> ObjectsToSpawn = new List<GameObject>();

    private void Awake()
    {
        if (ObjectsToSpawn != null && ObjectsToSpawn.Count > 0)
            Instantiate(ObjectsToSpawn.GetRandom(), transform.position, Quaternion.identity);
        else
            Debug.LogWarning($"{nameof(SpawnRandom)} no objects to spawn.", gameObject);

        Destroy(this);
    }
}
