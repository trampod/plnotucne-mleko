using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventProvider : MonoBehaviour
{
	public UnityEvent OnAwake;

	public UnityEvent OnStart;

	public UnityEvent OnUpdate;

	public UnityEvent OnLateUpdate;

	public UnityEvent OnFixedUpdate;

	public UnityEvent OnOnDestroy;

	public UnityEvent OnOnEnable;

	public UnityEvent OnOnDisable;


	private void Awake()
	{
		if (OnAwake != null)
			OnAwake.Invoke();
	}

	private void Start()
    {
        if(OnStart != null)
			OnStart.Invoke();
    }

    private void Update()
    {
        if(OnUpdate != null)
			OnUpdate.Invoke();
    }

	private void LateUpdate()
	{
		if(OnLateUpdate != null)
			OnLateUpdate.Invoke();
	}

	private void FixedUpdate()
	{
		if(OnFixedUpdate != null)
			OnFixedUpdate.Invoke();
	}

	private void OnDestroy()
	{
		if(OnOnDestroy != null)
			OnOnDestroy.Invoke();
	}

	private void OnEnable()
	{
		if(OnOnEnable != null)
			OnOnEnable.Invoke();
	}

	private void OnDisable()
	{
		if(OnOnDisable != null)
			OnOnDisable.Invoke();
	}
}
