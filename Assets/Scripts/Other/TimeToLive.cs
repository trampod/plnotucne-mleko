using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeToLive : MonoBehaviour
{
	[field: SerializeField]
	public bool IsCounting { get; private set; } = false;

	[field: SerializeField]
	[field: Tooltip("Expected time to live in seconds.")]
	public float Countdown { get; private set; } = 10f;

	[field: SerializeField]
	[field: ReadOnly]
	[field: Tooltip("Current time left in seconds.")]
    public float Counter { get; private set; }

	private void Awake()
	{
		if(IsCounting)
		{
			StartCountdown(Countdown);
		}
	}

	void Update()
    {
        if(IsCounting)
		{
			Counter -= Time.deltaTime;
			if(Counter <= 0)
			{
				Destroy(gameObject);
			}
		}
    }

	public void InitializeCountdown(float countdown)
	{
		Countdown = countdown;
		Counter = Countdown;
	}

	public void StartCountdown()
	{
		Counter = Countdown;
		IsCounting = true;
	}

	public void StartCountdown(float countdown)
	{
		InitializeCountdown(countdown);
		IsCounting = true;
	}

	public void ResetCountdown()
	{
		Counter = Countdown;
	}

	public void PauseCountdown()
	{
		IsCounting = false;
	}

	public void ResumeCountdown()
	{
		IsCounting = true;
	}
}
