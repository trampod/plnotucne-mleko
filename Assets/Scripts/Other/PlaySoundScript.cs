using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundScript : MonoBehaviour
{
    [Tooltip("List of AudioClips that should be played. When played a random clip from the list is selected.")]
    public List<AudioClip> Clips;

    [Tooltip("If it should automaticaly play the sound on Awake.")]
    public bool PlayOnAwake = true;

    [Tooltip("If it should destroy itself after playing the sound. The object is destroyed imediately, but the sound will still play in it's entirety.")]
    public bool DestroyAfter = false;

    private void Awake()
    {
        if (PlayOnAwake)
            Play();
    }

    public void Play()
    {
        if(Clips != null && Clips.Count > 0)
            AudioSource.PlayClipAtPoint(Clips.GetRandom(), transform.position);
        else
            Debug.LogWarning($"{nameof(PlaySoundScript)} on {gameObject.name} is missing AudioClips and therefore the sound can't be played.", gameObject);

        if(DestroyAfter)
            Destroy(gameObject);
    }
}
