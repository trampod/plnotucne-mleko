using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChange : MonoBehaviour
{
    [Scene]
    public string SceneName;

    [Tooltip("How long before changing the scenes.")]
    public float ChangeDelay = 0f;

    public void ChangeScenes()
    {
        Invoke(nameof(LoadScene), ChangeDelay);
    }

    private void LoadScene()
    {
        SceneManager.LoadScene(SceneName);
    }
}
