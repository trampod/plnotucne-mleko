using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInitializaton : MonoBehaviour
{
    [Tooltip("Ignores Game Initialization")]
    public bool Ignore;

    public DungeonGenerator Generator;

    public GameObject PlayerComposite;
    public GameObject Player;

    private void Awake()
    {
        MilkManager.Instance.Reset();

        Generator.Seed = Random.Range(0, 1000000);

        Generator.GenerateDungeon();

        List<MilkItem> milkItems = new List<MilkItem>(FindObjectsOfType<MilkItem>());

        PlayerComposite.SetActive(true);

        Player.transform.position = milkItems.GetRandom().transform.position;
    }
}
