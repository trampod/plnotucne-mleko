using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static Delaunay;

public static class Graph
{
    public static void Skeleton(List<Edge> graph, out List<Edge> skeleton, out List<Edge> remaining)
    {
        List<Edge> ordered = graph.OrderBy(x => Vector3.Distance(x.V.Position,x.U.Position)).ToList();
        HashSet<Vertex> vertices = new HashSet<Vertex>();
        skeleton = new List<Edge>();
        remaining = new List<Edge>();

        Queue<Edge> queue = new Queue<Edge>();

        foreach (Edge e in ordered)
        {
            queue.Enqueue(e);
        }

        while (queue.Count > 0)
        {
            Edge e = queue.Dequeue();
            if(vertices.Count != 0 && !vertices.Contains(e.V) && !vertices.Contains(e.U))
            {
                queue.Enqueue(e); // this will result in not entirely minimal spanning tree, but it's close and that's enough for me
            }
            else if(vertices.Contains(e.V) && vertices.Contains(e.U))
            {
                remaining.Add(e);
            }
            else
            {
                vertices.Add(e.V);
                vertices.Add(e.U);
                skeleton.Add(e);
            }
        }
    }
}
