using DataStructures.PriorityQueue;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AStar<TPosition>
{

    private class Node
    {
        public TPosition Position;
        public float Cost;
        public Node Parent;

        public Node(TPosition position, float cost, Node parent = null)
        {
            Position = position;
            Cost = cost;
            Parent = parent;
        }
    }

    public static List<TPosition> Search(
        List<TPosition> startPositions, 
        Func<TPosition, bool> isEndPositions,
        Func<TPosition, float> heuristicFunction,
        Func<TPosition, List<TPosition>> neighboursFunction,
        Func<TPosition, float> costFunction
        )
    {
        int queueCount = 0;
        HashSet<TPosition> visited = new HashSet<TPosition>();
        PriorityQueue<Node, float> queue = new PriorityQueue<Node, float>(0);
        foreach (var pos in startPositions)
        {
            queue.Insert(new Node(pos, 0), heuristicFunction(pos));
            queueCount++;
        }

        Node finalNode = null;

        while (queueCount > 0)
        {
            Node node = queue.Pop();
            queueCount--;
            while (visited.Contains(node.Position))
            {
                node = queue.Pop();
                queueCount--;
                if(queueCount == 0)
                    return null;
            }
            visited.Add(node.Position);

            if (isEndPositions(node.Position))
            {
                finalNode = node;
                break;
            }

            List<TPosition> neighbours = neighboursFunction(node.Position);

            foreach (TPosition item in neighbours)
            {
                float cost = node.Cost + costFunction(item);

                queue.Insert(new Node(item, cost, node), cost + heuristicFunction(item));
                queueCount++;
            }
        }

        List<TPosition> path = new List<TPosition>();

        while (finalNode != null)
        {
            path.Add(finalNode.Position);
            finalNode = finalNode.Parent;
        }

        return path;
    }
}
