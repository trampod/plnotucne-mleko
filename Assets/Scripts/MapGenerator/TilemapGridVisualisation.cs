using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TilemapGridVisualisation : GeneralVisualisation
{
    [SerializeField]
    private DungeonGenerator dungeon;

    public Tilemap Tilemap;
    public TileBase FloorTile;
    public TileBase WallTile;
    public TileBase HallTile;
    public TileBase DoorTile;

    [ContextMenu("Regenerate")]
    public override void RegenerateVisualisation()
    {
        ClearVisualisation();

        for (int x = 0; x < dungeon.DungeonSize.x; x++)
        {
            for (int y = 0; y < dungeon.DungeonSize.y; y++)
            {
                if(dungeon.DungeonGrid[x, y] != DungeonTile.Empty)
                {
                    TileBase tile = null;
                    switch (dungeon.DungeonGrid[x, y])
                    {
                        case DungeonTile.Hall:
                            tile = HallTile;
                            break;
                        case DungeonTile.Room:
                            tile = FloorTile;
                            break;
                        case DungeonTile.Door:
                            tile = DoorTile;
                            break;
                        case DungeonTile.Wall:
                            tile = WallTile;
                            break;
                        case DungeonTile.Reserved:
                            break;
                        case DungeonTile.Prop:
                            break;
                        case DungeonTile.Entrance:
                            break;
                        case DungeonTile.Escape:
                            break;
                        default:
                            break;
                    }
                    if(tile != null)
                    {
                        Tilemap.SetTile(new Vector3Int(x, y, 0), tile);
                    }
                }
            }
        }

        foreach(var room in dungeon.Rooms)
        {
            foreach(var pos in room.PlacedProps)
            {
                Instantiate(room.roomDefinition.PropList.GetRandomElement(),pos,Quaternion.identity,room.transform);
            }

            foreach (var pos in room.PlacedEnemies)
            {
                Instantiate(dungeon.EnemyList.GetRandomElement(), pos, Quaternion.identity, room.transform);
            }
        }

        //Tilemap.SetTile(new Vector3Int(0, 0, 0), WallTile);
        //Tilemap.SetTile(new Vector3Int(0, dungeon.DungeonSize.y - 1, 0), FloorTile);
        //Tilemap.SetTile(new Vector3Int(dungeon.DungeonSize.x - 1, 0, 0), DoorTile);
        //Tilemap.SetTile(new Vector3Int(dungeon.DungeonSize.x - 1, dungeon.DungeonSize.y - 1, 0), HallTile);
    }

    public override void ClearVisualisation()
    {
        Tilemap.ClearAllTiles();
    }
}
