using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using static Delaunay;

public enum DungeonTile
{
    Empty,
    Hall,
    Room,
    Door,
    Wall,
    Reserved,
    Prop,
    Entrance,
    Escape
}

// This code is a mess... taken from previous project and changed for this one... I did not have strength to do much cleaning, sorry for that
public class DungeonGenerator : MonoBehaviour
{
    [Separator("Debug")]
    public bool HideGUILayout;

    [Separator("Generator - Resources")]

    public List<RoomDefinition> PossibleRooms;

    public ProbabilityList EnemyList;


    [Separator("Generator - Helpers")]
    [SerializeField]
    public Pathfinder Pathfinder;

    public GeneralVisualisation gridVisualisation;


    [Separator("Generator - Settings")]

    public int Seed = -1;

    [MinMaxRange(5, 20)]
    public RangedInt RoomCount = new(8, 12);

    public int NumberOfTries;

    public Vector2Int DungeonSize = new Vector2Int(20, 20);
    public float edgeSaveChance = 0.5f;


    [Separator]
    [Header("Generator - Outcome")]

    [ReadOnly]
    public int FinalRoomCount;

    [SerializeField]
    public List<Room> Rooms = new List<Room>();


    public DungeonTile[,] DungeonGrid;




    private List<Edge> graph;
    private List<Vertex> allVectors;
    private GameObject mapHolder;
    private GameObject modelsHolder;

    void Awake()
    {
        if (gridVisualisation == null)
            gridVisualisation = FindObjectOfType<GeneralVisualisation>();

        //Generate();
    }

    private void OnGUI()
    {
        if (HideGUILayout)
            return;

        GUILayout.Label($"Seed = {Seed}");
        if (GUILayout.Button("Reset Seed"))
        {
            Seed = -1;
            _nextStep = 0;
        }
        GUILayout.Label("");
        GUILayout.Label($"Size = {DungeonSize}");
        GUILayout.Label($"Rooms = {FinalRoomCount}");
        if (GUILayout.Button("Start New"))
        {
            Seed++;
            Random.InitState(Seed);
            ClearDungeon();
            //Generate();
            _nextStep = 0;
            NextGenerationStep();
            if (gridVisualisation != null)
                gridVisualisation.RegenerateVisualisation();
            graph = null;
        }

        if (_nextStep > 0)
        {
            if (GUILayout.Button("Next Step"))
            {
                NextGenerationStep();
                if (gridVisualisation != null)
                    gridVisualisation.RegenerateVisualisation();
            }
        }
        else
        {
            GUILayout.Label("");
        }

        if (GUILayout.Button("Generate Complete New"))
        {
            Seed++;
            Random.InitState(Seed);
            ClearDungeon();
            GeneratePreview();
            CleanTiles();
            _nextStep = 0;
            if (gridVisualisation != null)
                gridVisualisation.RegenerateVisualisation();
            previewGenerated = true;
        }
    }

    private bool previewGenerated = false;

    public void GeneratePreview()
    {
        GeneratorInit();
        GenerateRooms();
        GenerateGraph();
        PruneGraph();
        ClearReserved();
        GenerateFullFloorPaths();
        GenerateRoomTypes();
        //GenerateModels();

        //gridVisualisation.RegenerateVisualisation();
    }

    public void GenerateDungeon()
    {
        GenerateDungeon(Seed);
    }

    public void GenerateDungeon(int seed)
    {
        Random.InitState(Seed);
        ClearDungeon();
        GeneratePreview();
        CleanTiles();
        if (gridVisualisation != null)
            gridVisualisation.RegenerateVisualisation();
    }

    private int _nextStep;

    [ContextMenu(nameof(NextGenerationStep))]
    public void NextGenerationStep()
    {
        switch (_nextStep)
        {
            case 0:
                GeneratorInit();
                break;
            case 1:
                GenerateRooms();
                break;
            case 2:
                GenerateGraph();
                break;
            case 3:
                PruneGraph();
                break;
            case 4:
                ClearReserved();
                GenerateFullFloorPaths();
                break;
            case 5:
                GenerateRoomTypes();
                previewGenerated = true;
                _nextStep = -1;
                break;
            //case 6:
            //    GenerateModels();
            //    break;
            default:
                _nextStep = 0;
                return;
        }


        _nextStep++;
    }

    private List<System.Tuple<Vector2Int, DungeonTile>> TileNeighbours(Vector2Int position)
    {
        List<System.Tuple<Vector2Int, DungeonTile>> tuples = new List<System.Tuple<Vector2Int, DungeonTile>>();

        List<Vector2Int> positions = new List<Vector2Int>();
        List<Vector2Int> fakePos = new List<Vector2Int>();
        Vector2Int[] axisX = new Vector2Int[3] { Vector2Int.left, Vector2Int.zero, Vector2Int.right };
        Vector2Int[] axisY = new Vector2Int[3] { Vector2Int.down, Vector2Int.zero, Vector2Int.up };

        foreach (var vecX in axisX)
        {
            foreach(var vecY in axisY)
            {
                Vector2Int vector = vecX + vecY + position;
                if (vector.x >= 0 && vector.x < DungeonSize.x && vector.y >= 0 && vector.y < DungeonSize.y)
                    positions.Add(vector);
                else
                    fakePos.Add(vector);
            }
        }

        positions.Remove(position);
        fakePos.Remove(position);

        foreach (Vector2Int pos in positions)
        {
            tuples.Add(new System.Tuple<Vector2Int, DungeonTile>(pos, DungeonGrid[pos.x, pos.y]));
        }
        foreach (Vector2Int pos in fakePos)
        {
            tuples.Add(new System.Tuple<Vector2Int, DungeonTile>(pos, DungeonTile.Empty));
        }

        return tuples;
    }

    private List<Vector2Int> Neighbours(Vector2Int position)
    {
        List<Vector2Int> result = new List<Vector2Int>();
        Vector2Int[] axisX = new Vector2Int[3] { Vector2Int.left, Vector2Int.zero, Vector2Int.right };
        Vector2Int[] axisY = new Vector2Int[3] { Vector2Int.down, Vector2Int.zero, Vector2Int.up };

        foreach (var vecX in axisX)
        {
            foreach (var vecY in axisY)
            {
                Vector2Int vector = vecX + vecY + position;
                if (vector.x >= 0 && vector.x < DungeonSize.x && vector.y >= 0 && vector.y < DungeonSize.y)
                    result.Add(vector);
            }
        }

        result.Remove(position);

        return result;
    }


    private void GenerateRoomTypes()
    {
        foreach (Room room in Rooms)
        {
            room.PlaceProps();
            room.PlaceEnemies();
        }

        List<Room> access = Rooms.GetRandom(2);
        access[0].SetAccess(AccessType.Entrance);
        access[1].SetAccess(AccessType.Escape);
    }

    private void PruneGraph()
    {
        List<Edge> skeleton;
        List<Edge> rermaining;

        Graph.Skeleton(graph, out skeleton, out rermaining);

        graph = skeleton;

        foreach (Edge e in rermaining)
        {
            if (Random.Range(0f, 1f) < edgeSaveChance)
            {
                graph.Add(e);
            }
        }
    }

    private void GeneratorInit()
    {
        mapHolder = new GameObject("Map Holder");
        mapHolder.transform.parent = transform;
        modelsHolder = new GameObject("Models Holder");
        modelsHolder.transform.parent = transform;
        DungeonGrid = new DungeonTile[DungeonSize.x, DungeonSize.y];
        FinalRoomCount = Random.Range(RoomCount.Min, RoomCount.Max + 1);
        previewGenerated = false;
        if (gridVisualisation != null)
            gridVisualisation.gameObject.SetActive(true);
    }

    private void ClearReserved()
    {
        for (int x = 0; x < DungeonSize.x; x++)
        {
            for (int y = 0; y < DungeonSize.y; y++)
            {
                if (DungeonGrid[x, y] == DungeonTile.Reserved)
                    DungeonGrid[x, y] = DungeonTile.Empty;
            }
        }
    }

    private void GenerateRooms()
    {
        for (int i = 0; i < FinalRoomCount; i++)
        {
            for (int j = 0; j < NumberOfTries; j++)
            {
                if (PlaceRoom())
                    break;
            }
        }
    }

    private bool PlaceRoom()
    {
        RoomDefinition room = PossibleRooms.GetRandom();

        int rotation = Random.Range(0, 4);
        bool willRotate = rotation % 2 > 0;
        bool mirrorX = Random.Range(0, 2) > 0;
        bool mirrorZ = Random.Range(0, 2) > 0;

        Vector3Int position = new Vector3Int(
            Random.Range(1, DungeonSize.x - (willRotate ? room.RealRoomSize.y : room.RealRoomSize.x) - 1),
            Random.Range(1, DungeonSize.y - (willRotate ? room.RealRoomSize.x : room.RealRoomSize.y) - 1));

        if (!CanPlace(room, position, willRotate))
            return false;


        GameObject go = new GameObject($"Room-{Rooms.Count}");
        go.transform.position = position;
        go.transform.rotation = transform.rotation;
        go.transform.parent = mapHolder.transform;
        Room rt = go.AddComponent<Room>();
        rt.roomDefinition = room;

        rt.Rotated = willRotate;

        rt.Rotation = rotation;
        rt.MirrorX = mirrorX;
        rt.MirrorZ = mirrorZ;
        rt.Position = position.xy();

        rt.SetTiles(ref DungeonGrid);

        Rooms.Add(rt);
        return true;
    }

    private bool CanPlace(RoomDefinition room, Vector3Int position, bool rotate)
    {
        //Debug.Log("CanPlace()");
        //Debug.Log($"room.Size={room.Size}");
        //Debug.Log($"position={position}");
        //Debug.Log($"rotate={rotate}");
        int SizeX = rotate ? room.RealRoomSize.y : room.RealRoomSize.x;
        int SizeY = rotate ? room.RealRoomSize.x : room.RealRoomSize.y;
        for (int x = 0; x < SizeX; x++)
        {
            for (int y = 0; y < SizeY; y++)
            {
                //Debug.Log($"xyz={x},{y}");
                if (DungeonGrid[position.x + x, position.y + y] != DungeonTile.Empty)
                    return false;
            }
        }
        return true;
    }

    private void CleanTiles()
    {
        for (int x = 0; x < DungeonSize.x; x++)
        {
            for (int y = 0; y < DungeonSize.y; y++)
            {
                DungeonTile tile = DungeonGrid[x, y];
                switch (tile)
                {
                    case DungeonTile.Empty:
                        break;
                    case DungeonTile.Hall:
                        SurroundWalls(x, y);
                        break;
                    case DungeonTile.Room:
                        break;
                    case DungeonTile.Door:
                        CheckDoor(x, y);
                        break;
                    case DungeonTile.Wall:
                        break;
                    case DungeonTile.Reserved:
                        break;
                    case DungeonTile.Prop:
                        break;
                    case DungeonTile.Entrance:
                        break;
                    case DungeonTile.Escape:
                        break;
                }
            }
        }
    }

    private void CheckDoor(int x, int y)
    {
        if(DungeonGrid[x, y] == DungeonTile.Door && !canPlaceDoorH(x, y) && !canPlaceDoorI(x, y))
        {
            DungeonGrid[x, y] = DungeonTile.Wall;
        }
    }

    private bool canPlaceDoorH(int x, int y)
    {
        return (x > 0 && isWalkable(DungeonGrid[x-1, y]))
            && (x < DungeonSize.x - 1 && isWalkable(DungeonGrid[x+1, y]));
    }

    private bool isWalkable(DungeonTile dungeonTile)
    {
        return dungeonTile == DungeonTile.Room || dungeonTile == DungeonTile.Hall;
    }

    private bool canPlaceDoorI(int x, int y)
    {
        return (y > 0 && isWalkable(DungeonGrid[x, y-1]))
            && (y < DungeonSize.y - 1 && isWalkable(DungeonGrid[x, y+1]));
    }

    private void SurroundWalls(int x, int y)
    {
        List<Vector2Int> list = Neighbours(new Vector2Int(x,y));

        foreach(Vector2Int v in list)
        {
            if(DungeonGrid[v.x,v.y] == DungeonTile.Empty)
            {
                DungeonGrid[v.x, v.y] = DungeonTile.Wall;
            }
        }

    }

    private void GenerateGraph()
    {
        graph = new List<Edge>();
        allVectors = new List<Vertex>();

        List<Vertex> vectors = new List<Vertex>();

        foreach (Room room in Rooms)
        {
            vectors.Add(new Vertex<Room>(room.middlePoint, room));
        }

        //Debug.Log(vectors.Count);
        if (vectors.Count == 2)
            graph.Add(new Edge(vectors[0], vectors[1]));
        if (vectors.Count == 3)
        {
            graph.Add(new Edge(vectors[0], vectors[1]));
            graph.Add(new Edge(vectors[1], vectors[2]));
            graph.Add(new Edge(vectors[2], vectors[0]));
        }
        else if (vectors.Count > 3)
        {
            Delaunay delaunay = Delaunay.Triangulate(vectors);
            //Debug.Log($"delunay.edges.count={delaunay.Edges.Count}");
            graph.AddRange(delaunay.Edges);
        }

        //allVectors.AddRange(vectors);
    }

    private List<Room>[] GetFloorRooms()
    {
        List<Room>[] floors = new List<Room>[DungeonSize.y];

        for (int i = 0; i < DungeonSize.y; i++)
        {
            floors[i] = new List<Room>();
        }

        foreach (Room room in Rooms)
        {
            List<int> doorFloors = new List<int>();

            // I only want to make the graph for floors the room has doors on... left from time there were suposed to be tall rooms... which for now there won't... yet
            List<Vector2Int> doorPositions = room.GetDoorTiles();
            foreach (Vector2Int door in doorPositions)
            {
                if (!doorFloors.Contains(door.y))
                {
                    doorFloors.Add(door.y);
                }
            }

            foreach (int floor in doorFloors)
            {
                //Debug.Log($"floor={floor}");
                floors[floor].Add(room);
            }
        }

        return floors;
    }

    private void GenerateFullFloorPaths()
    {

        foreach (Edge edge in graph)
        {
            //Debug.Log($"edge.V={edge.V.GetType()}, edge.U={edge.U.GetType()}");
            Vertex<Room> v = edge.V as Vertex<Room>;
            Vertex<Room> u = edge.U as Vertex<Room>;
            //Debug.Log($"edge.V.item={v.Item}, edge.U={u.Item}");

            List<Vector2Int> path = Pathfinder.CreatePath(u.Item, v.Item);

            if(path == null)
                continue; // ignore if no possible path exists... should not happen due to generator design (at least not any more)

            foreach (Vector2Int p in path)
            {
                if (DungeonGrid[p.x, p.y] == DungeonTile.Empty || DungeonGrid[p.x, p.y] == DungeonTile.Reserved)
                    DungeonGrid[p.x, p.y] = DungeonTile.Hall;
            }
        }
    }

    public void ClearDungeon()
    {
        if (mapHolder != null)
        {
            Destroy(mapHolder);
            mapHolder = null;
        }
        if (modelsHolder != null)
        {
            Destroy(modelsHolder);
            modelsHolder = null;
        }
        Rooms.Clear();
        if (gridVisualisation != null)
            gridVisualisation.ClearVisualisation();
    }

    private void OnDrawGizmos()
    {
        if (gridVisualisation == null)
            return;

        //Debug.Log(graph == null);
        if (graph != null)
        {
            //Debug.Log(graph.Count);
            Gizmos.color = Color.yellow;
            foreach (Edge edge in graph)
            {
                //Debug.Log(edge.U + "x" + edge.V);
                Gizmos.DrawLine(edge.U.Position, edge.V.Position);
            }
        }

        if (allVectors != null)
        {

            Gizmos.color = Color.cyan;
            foreach (Vertex vector in allVectors)
            {
                Gizmos.DrawSphere(vector.Position, 1);
            }
        }

        //Gizmos.color = Color.yellow;
        //foreach(RoomTemplate room in Rooms)
        //{
        //    foreach (Vector3 prop in room.PlacedProps)
        //    {
        //        Gizmos.DrawSphere(prop+(Vector3.one/2), 0.5f);
        //    }
        //}
    }
}
