using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraController : MonoBehaviour
{
    public float MovementSpeed = 10f;

    private Vector2 moveVector = Vector2.zero;

    private void Update()
    {
        Vector2 vector = moveVector * MovementSpeed * Time.deltaTime;
        this.transform.position += new Vector3(vector.x, vector.y);
    }

    public void OnMovement(InputAction.CallbackContext context)
    {
        moveVector = context.ReadValue<Vector2>();
    }
}
