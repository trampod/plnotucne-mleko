using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RoomType
{
    GeneralRoom, // tables, cabinets
    Armory, // weapon stands
    LivingQuarters // beds
}

public enum PropType
{
    Entrance, // hole in the ceiling
    Escape, // hole in the floor
    GeneralRoom, // tables, cabinets
    Armory, // weapon stands
    LivingQuarters // beds
}

public enum AccessType
{
    Normal,
    Entrance, // hole in the ceiling
    Escape // hole in the floor
}

public class Room : MonoBehaviour
{
    public int Rotation = 0;
    public bool MirrorX = false;
    public bool MirrorZ = false;
    public bool Rotated = false;

    public RoomDefinition roomDefinition;

    public Vector2Int Position;

    public Vector3 middlePoint => new Vector3(
        transform.position.x + ((float)(Rotated ? roomDefinition.RealRoomSize.y : roomDefinition.RealRoomSize.x) - 1) / 2,
        transform.position.y + ((float)(Rotated ? roomDefinition.RealRoomSize.x : roomDefinition.RealRoomSize.y) - 1) / 2);

    public RoomType Type = RoomType.GeneralRoom;
    public AccessType AccessType = AccessType.Normal;

    [SerializeField, ReadOnly]
    public List<Vector3> PlacedProps = new List<Vector3>();

    [SerializeField, ReadOnly]
    public List<Vector3> PlacedEnemies = new List<Vector3>();

    public DungeonTile GetTile(Vector2Int pos)
    {
        foreach (var tile in roomDefinition.DoorPositions)
        {
            if (tile == pos)
                return DungeonTile.Door;
        }

        RoomTile roomTile = roomDefinition.GetTile(pos);

        switch(roomTile)
        {
            case RoomTile.Wall:
                return DungeonTile.Wall;
            case RoomTile.Floor:
                return DungeonTile.Room;
            default:
                return DungeonTile.Empty;
        }

    }

    public List<Vector2Int> GetDoorTiles()
    {
        List<Vector2Int> result = new List<Vector2Int>();
        foreach (var tile in roomDefinition.DoorPositions)
        {
            result.Add(GetFinalPosition(tile) + Position);
        }
        return result;
    }

    private void OnDrawGizmos()
    {
        //Gizmos.color = Color.white;
        //Gizmos.DrawSphere(transform.position, 1f);
        //Gizmos.color = Color.red;
        //Gizmos.DrawSphere(middlePoint, 1f);
    }

    public void SetTiles(ref DungeonTile[,] dungeonGrid)
    {
        for (int x = 0; x < roomDefinition.RealRoomSize.x; x++)
        {
            for (int y = 0; y < roomDefinition.RealRoomSize.y; y++)
            {
                Vector2Int pos, tilePos = new Vector2Int(x, y);

                pos = GetFinalPosition(tilePos);

                dungeonGrid[Position.x + pos.x, Position.y + pos.y] = GetTile(tilePos);
            }
        }

        for (int x = -1; x <= roomDefinition.RealRoomSize.x; x++)
        {
            Vector2Int pos1, tilePos1 = new Vector2Int(x, roomDefinition.RealRoomSize.y);
            Vector2Int pos2, tilePos2 = new Vector2Int(x, -1);

            pos1 = GetFinalPosition(tilePos1);
            pos2 = GetFinalPosition(tilePos2);

            dungeonGrid[Position.x + pos1.x, Position.y + pos1.y] = DungeonTile.Reserved;
            dungeonGrid[Position.x + pos2.x, Position.y + pos2.y] = DungeonTile.Reserved;
        }

        for (int y = -1; y <= roomDefinition.RealRoomSize.y; y++)
        {
            Vector2Int pos1, tilePos1 = new Vector2Int(-1, y);
            Vector2Int pos2, tilePos2 = new Vector2Int(roomDefinition.RealRoomSize.x, y);

            pos1 = GetFinalPosition(tilePos1);
            pos2 = GetFinalPosition(tilePos2);

            dungeonGrid[Position.x + pos1.x, Position.y + pos1.y] = DungeonTile.Reserved;
            dungeonGrid[Position.x + pos2.x, Position.y + pos2.y] = DungeonTile.Reserved;
        }
    }

    internal void SetAccess(AccessType access)
    {
        AccessType = access;

        int index = UnityEngine.Random.Range(0, PlacedProps.Count);
        //PlacedProps[index] = new Tuple<Vector3, PropType>(PlacedProps[index].Item1, PropManager.GetPropType(access));
    }

    public void PlaceProps()
    {
        foreach(Vector2Int propPos in roomDefinition.PropPositions)
        {
            Vector2Int pos;

            pos = GetFinalPosition(propPos);

            Vector3 placement = new Vector3(Position.x + pos.x + 0.5f, Position.y + pos.y + 0.5f);

            PlacedProps.Add(placement);
        }
    }

    public void PlaceEnemies()
    {
        foreach (Vector2Int enemyPos in roomDefinition.EnemyPositions)
        {
            Vector2Int pos;

            pos = GetFinalPosition(enemyPos);

            Vector3 placement = new Vector3(Position.x + pos.x + 0.5f, Position.y + pos.y + 0.5f);

            PlacedEnemies.Add(placement);
        }
    }

    private Vector2Int GetFinalPosition(int x, int y)
    {
        Vector2Int pos;
        switch (Rotation)
        {
            case 3:
                pos = new Vector2Int(y, roomDefinition.RealRoomSize.x - x - 1);
                break;
            case 2:
                pos = new Vector2Int(roomDefinition.RealRoomSize.x - x - 1, roomDefinition.RealRoomSize.y - y - 1);
                break;
            case 1:
                pos = new Vector2Int(roomDefinition.RealRoomSize.y - y - 1, x);
                break;
            default:
                pos = new Vector2Int(x, y);
                break;
        }
        pos = new Vector2Int(
            MirrorX ? (Rotation % 2 == 0 ? roomDefinition.RealRoomSize.x : roomDefinition.RealRoomSize.y) - 1 - pos.x : pos.x,
            MirrorZ ? (Rotation % 2 == 0 ? roomDefinition.RealRoomSize.y : roomDefinition.RealRoomSize.x) - 1 - pos.y : pos.y);

        return pos;
    }

    private Vector2Int GetFinalPosition(Vector2Int position)
    {
        return GetFinalPosition(position.x, position.y);
    }
}
