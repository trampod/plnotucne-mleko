using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TilemapShadowVisualisation : GeneralVisualisation
{
    [SerializeField]
    private DungeonGenerator dungeon;

    public Tilemap BaseTilemap;
    [Tooltip("This defines walls used for shadows.")]
    public Tilemap WallTilemap;

    [Separator("Tiles")]
    public TileBase FloorTile;
    public TileBase WallTile;
    public TileBase HallTile;
    public TileBase DoorTile;

    [ButtonMethod]
    public override void RegenerateVisualisation()
    {
        ClearVisualisation(false);

        for (int x = 0; x < dungeon.DungeonSize.x; x++)
        {
            for (int y = 0; y < dungeon.DungeonSize.y; y++)
            {
                if (dungeon.DungeonGrid[x, y] != DungeonTile.Empty)
                {
                    TileBase tile = null;
                    switch (dungeon.DungeonGrid[x, y])
                    {
                        case DungeonTile.Hall:
                            tile = HallTile;
                            break;
                        case DungeonTile.Room:
                            tile = FloorTile;
                            break;
                        case DungeonTile.Door:
                            tile = DoorTile;
                            break;
                        case DungeonTile.Wall:
                            tile = WallTile;
                            var neighbours = Neighbours(x,y);
                            bool corner = true;
                            foreach(var item in neighbours)
                            {
                                var otherTile = dungeon.DungeonGrid[item.x, item.y];
                                if (otherTile != DungeonTile.Wall && otherTile != DungeonTile.Empty)
                                    corner = false;
                            }
                            if(!corner)
                            {
                                WallTilemap.SetTile(new Vector3Int(x, y, 0), tile);
                            }
                            break;
                        case DungeonTile.Reserved:
                        case DungeonTile.Prop:
                        case DungeonTile.Entrance:
                        case DungeonTile.Escape:
                        default:
                            break;
                    }
                    if (tile != null)
                    {
                        BaseTilemap.SetTile(new Vector3Int(x, y, 0), tile);
                    }
                }
            }
        }

        foreach (var room in dungeon.Rooms)
        {
            foreach (var pos in room.PlacedProps)
            {
                Instantiate(room.roomDefinition.PropList.GetRandomElement(), pos, Quaternion.identity, room.transform);
            }

            foreach (var pos in room.PlacedEnemies)
            {
                Instantiate(dungeon.EnemyList.GetRandomElement(), pos, Quaternion.identity, room.transform);
            }
        }

        //this was needed with delay, because otherwise it didn't rebuild correctly
        Invoke(nameof(ResetShadows),0.5f);
    }

    [ButtonMethod]
    public override void ClearVisualisation()
    {
        ClearVisualisation(true);
    }

    private void ClearVisualisation(bool resetShadows)
    {
        BaseTilemap.ClearAllTiles();


        for (int x = 0; x < dungeon.DungeonSize.x; x++)
        {
            for (int y = 0; y < dungeon.DungeonSize.y; y++)
            {
                WallTilemap.SetTile(new Vector3Int(x, y, 0), null);
            }
        }

        if(resetShadows)
            ShadowCaster2DFromComposite.RebuildAll();
    }

    [ButtonMethod]
    public void ResetShadows()
    {
        ShadowCaster2DFromComposite.RebuildAll();
    }

    private List<Vector2Int> Neighbours(int x, int y)
    {
        var position = new Vector2Int(x, y);

        List<Vector2Int> result = new List<Vector2Int>();

        result.Add(position + Vector2Int.up);
        result.Add(position + Vector2Int.down);
        result.Add(position + Vector2Int.right);
        result.Add(position + Vector2Int.left);

        for (int i = result.Count - 1; i >= 0; i--)
        {
            if (result[i].x < 0 ||
                result[i].y < 0 ||
                result[i].x >= dungeon.DungeonSize.x ||
                result[i].y >= dungeon.DungeonSize.y)
            {
                result.RemoveAt(i);
            }
        }

        return result;
    }
}
