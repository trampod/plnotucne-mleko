using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ProbabilityElement
{
    public GameObject gameObject;
    public float chance;
    public float weight;

    public ProbabilityElement(GameObject gameObject, float weight)
    {
        this.gameObject = gameObject;
        this.chance = weight;
        this.weight = weight;
    }
}

[CreateAssetMenu(fileName = "ProbabilityList", menuName = "ScriptableObjects/ProbabilityList", order = 2)]
public class ProbabilityList : ScriptableObject
{
    public string ListName;

    public List<ProbabilityElement> ProbabilityItems = new List<ProbabilityElement>();
    

    public GameObject GetRandomElement()
    {
        float chance = UnityEngine.Random.Range(0.0f, 1.0f);

        foreach(var pair in ProbabilityItems)
        {
            if(chance < pair.chance)
                return pair.gameObject;
            chance -= pair.chance;
        }
        return null;
    }
}
