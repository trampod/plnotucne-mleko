using DataStructures.PriorityQueue;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Pathfinder : MonoBehaviour
{
    [SerializeField]
    public DungeonGenerator Generator;

    [Header("Tile Costs")]
    public float EmptyCost;
    public float HallCost;
    public float DoorCost;
    public float RoomCost;
    public float WallCost;

    public List<Vector2Int> CreatePath(Room start, Room end)
    {
        List<Vector2Int> endPositions = end.GetDoorTiles();
        List<Vector2Int> doorPositions = start.GetDoorTiles();

        return AStar<Vector2Int>.Search(
            doorPositions,
            x => endPositions.Contains(x),
            x => Heuristic(x,endPositions),
            Neighbours,
            Cost
            );
    }

    private float Cost(Vector2Int position)
    {
        DungeonTile tile = Generator.DungeonGrid[position.x, position.y];
        switch (tile)
        {
            case DungeonTile.Empty:
            case DungeonTile.Reserved:
                return EmptyCost;
            case DungeonTile.Hall:
                return HallCost;
            case DungeonTile.Room:
            case DungeonTile.Entrance:
            case DungeonTile.Escape:
            case DungeonTile.Prop:
                return RoomCost;
            case DungeonTile.Door:
                return DoorCost;
            case DungeonTile.Wall:
                return WallCost;
            default:
                break;
        }
        return 0;
    }

    private List<Vector2Int> Neighbours (Vector2Int nodePosition)
    {
        List<Vector2Int> result = new List<Vector2Int> ();

        result.Add(nodePosition + Vector2Int.up);
        result.Add(nodePosition + Vector2Int.down);
        result.Add(nodePosition + Vector2Int.right);
        result.Add(nodePosition + Vector2Int.left);

        for(int i = result.Count-1; i >= 0; i--)
        {
            if (result[i].x < 0 ||
                result[i].y < 0 ||
                result[i].x >= Generator.DungeonSize.x ||
                result[i].y >= Generator.DungeonSize.y ||
                Generator.DungeonGrid[result[i].x, result[i].y] == DungeonTile.Room)
            {
                result.RemoveAt(i);
            }
        }

        return result;
    }

    private float Heuristic(Vector2Int position, List<Vector2Int> endNodes)
    {
        float result = float.MaxValue;
        for (int i = 0; i < endNodes.Count; i++)
        {
            float dist = Vector2Int.Distance(position, endNodes[i]);
            if(dist < result)
                result = dist;
        }
        return result;
    }

    private class Node
    {
        public Vector2Int Position;
        public float Cost;
        public Node Parent;

        public Node(Vector2Int position, float cost, Node parent = null)
        {
            Position = position;
            Cost = cost;
            Parent = parent;
        }
    }
}
