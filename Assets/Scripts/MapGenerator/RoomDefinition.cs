using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum RoomTile
{
    Empty = 0,
    Floor = 1,
    Wall = 2,
}


[CreateAssetMenu(fileName = "Room", menuName = "ScriptableObjects/RoomDefinition", order = 1)]
public class RoomDefinition : ScriptableObject
{
    public Vector2Int RoomSize = new Vector2Int(3, 3);
    public Vector2Int RealRoomSize = new Vector2Int(5,5);

    public Vector2Int prevSize = new Vector2Int(3, 3);

    public RoomTile[] RoomTiles = new RoomTile[9];

    public List<Vector2Int> DoorPositions = new List<Vector2Int>();

    public ProbabilityList PropList = null;
    public List<Vector2Int> PropPositions = new List<Vector2Int>();

    public List<Vector2Int> EnemyPositions = new List<Vector2Int>();

    public RoomTile GetTile(int x, int y)
    {
        return RoomTiles[x + y * RealRoomSize.x];
    }

    public RoomTile GetTile(Vector2Int v)
    {
        return GetTile(v.x, v.y);
    }

    public void SetTile(int x, int y, RoomTile tile)
    {
        RoomTiles[x + y * RealRoomSize.x] = tile;
    }

    public void SetTile(Vector2Int v, RoomTile tile)
    {
        SetTile(v.x, v.y, tile);
    }

    public void SetFloor(int x, int y)
    {
        AddWalls(x, y);
        SetTile(x, y, RoomTile.Floor);
        CheckWalls();
    }

    private void CheckWalls()
    {
        for (int i = DoorPositions.Count - 1; i >= 0; i--)
        {
            Vector2Int door = DoorPositions[i];
            if (door.x >= RealRoomSize.x || door.y >= RealRoomSize.y || !CanPlaceDoor(door))
                DoorPositions.Remove(door);
        }
    }

    public void UnSetFloor(int x, int y)
    {
        Vector2Int v = new Vector2Int(x,y);
        if (PropPositions.Contains(v))
            PropPositions.Remove(v);
        if (EnemyPositions.Contains(v))
            EnemyPositions.Remove(v);
        SetTile(x, y, RoomTile.Wall);
        RemoveWalls(x, y);
        CheckWalls();
    }

    public void ResizeArray()
    {
        Debug.Log("RoomTiles Resizing");

        RoomTile[] roomTiles = new RoomTile[RealRoomSize.x * RealRoomSize.y];

        if (RoomTiles == null)
        {
            RoomTiles = roomTiles;
            return;
        }

        int copyX = Mathf.Min(RealRoomSize.x, prevSize.x);
        int copyY = Mathf.Min(RealRoomSize.y, prevSize.y);

        for (int x = 0; x < copyX; x++)
        {
            for (int y = 0; y < copyY; y++)
            {
                roomTiles[x + y * RealRoomSize.x] = RoomTiles[x + y * prevSize.x];
            }
        }

        RoomTiles = roomTiles;
        prevSize = RealRoomSize;


        for (int x = 0; x < RealRoomSize.x; x++)
        {
            UnSetFloor(x, 0);
            UnSetFloor(x, RealRoomSize.y - 1);
        }
        for (int y = 1; y < RealRoomSize.y - 1; y++)
        {
            UnSetFloor(0, y);
            UnSetFloor(RealRoomSize.x - 1, y);
        }

        for (int i = PropPositions.Count - 1; i >= 0; i--)
		{
            if (PropPositions[i].x > RoomSize.x || PropPositions[i].y > RoomSize.y)
                PropPositions.RemoveAt(i);
		}
        for (int i = EnemyPositions.Count - 1; i >= 0; i--)
		{
            if (EnemyPositions[i].x > RoomSize.x || EnemyPositions[i].y > RoomSize.y)
                EnemyPositions.RemoveAt(i);
		}
    }

    public void AddWalls(int x, int y)
    {
        var neighbours = Neighbours(x, y);
        foreach(var neighbor in neighbours)
        {
            if(GetTile(neighbor) == RoomTile.Empty)
                SetTile(neighbor, RoomTile.Wall);
        }
    }

    public void RemoveWalls(int x, int y)
    {
        var neighbours = Neighbours(x, y);
        foreach (var neighbor in neighbours)
        {
            if (GetTile(neighbor) == RoomTile.Wall)
            {
                var neighbours2 = Neighbours(neighbor);

                SetTile(neighbor, RoomTile.Empty);
                foreach (var neighbor2 in neighbours2)
                {
                    if (GetTile(neighbor2) == RoomTile.Floor)
                    {
                        SetTile(neighbor, RoomTile.Wall);
                        break;
                    }
                }
            }
        }
    }

    public List<Vector2Int> Neighbours(Vector2Int v)
    {
        return Neighbours(v.x,v.y);
    }

    public List<Vector2Int> Neighbours(int centerX, int centerY)
    {
        List<Vector2Int> result = new List<Vector2Int>();

        for (int x = -1; x < 2; x++)
        {
            for (int y = -1; y < 2; y++)
            {
                Vector2Int v = new Vector2Int(centerX + x, centerY + y);
                if(v.x >=0 && v.x < RealRoomSize.x && v.y >=0 && v.y < RealRoomSize.y)// && !(v.x == centerX && v.y == centerY))
                {
                    result.Add(v);
                }
            }
        }

        return result;
    }

    public bool CanPlaceDoor(int x, int y)
    {
        return GetTile(x, y) == RoomTile.Wall && (canPlaceDoorH(x,y) || canPlaceDoorI(x,y));
    }

    private bool canPlaceDoorH(int x, int y)
    {
        return (x ==0 || GetTile(x - 1,y) != RoomTile.Wall)
            && ( x == RealRoomSize.x - 1 || GetTile(x + 1,y) != RoomTile.Wall);
    }

    private bool canPlaceDoorI(int x, int y)
    {
        return (y == 0 || GetTile(x, y - 1) != RoomTile.Wall)
            && (y == RealRoomSize.y - 1 || GetTile(x, y + 1) != RoomTile.Wall);
    }

    public bool CanPlaceDoor(Vector2Int v)
    {
        return CanPlaceDoor(v.x, v.y);
    }

    public List<Vector2Int> CloseNeighbours(int centerX, int centerY)
    {
        List<Vector2Int> result = new List<Vector2Int>()
        {
            new Vector2Int(centerX +1, centerY),
            new Vector2Int(centerX, centerY+ 1),
            new Vector2Int(centerX -1, centerY),
            new Vector2Int(centerX, centerY- 1),
        };

        for (int i = result.Count-1; i >= 0; i--)
        {
            Vector2Int v = result[i];
            if (!(v.x >= 0 && v.x < RealRoomSize.x && v.y >= 0 && v.y < RealRoomSize.y))
            {
                result.Remove(v);
            }
        }

        return result;
    }
}
